package com.geeks.service;

public interface AwsSmsService {

    String send(String phoneNumber, int numbSize);

    String send(String phoneNumber, String message);
    
}
