package com.geeks.service;

import com.geeks.domain.Location;
import com.geeks.domain.Order;
import com.geeks.dto.StoreMinerDescriptionDTO;
import com.geeks.dto.StoreMinerListDTO;

import java.util.List;

public interface StoreService {
    List<StoreMinerListDTO> findAllMachines();
    List<Location> findMachineLocations(Long id);
    Order createOrder(Long machine_id,Long location_id,Long user_id);
    StoreMinerDescriptionDTO findMachineById(Long machine_id);
}
