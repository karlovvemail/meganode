package com.geeks.service;

import com.geeks.dto.myminers.MyMinersDTO;

public interface MyMinersService {

    MyMinersDTO getMinersByUser(Long id);

}
