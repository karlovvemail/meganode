package com.geeks.service;

import com.geeks.domain.profit.BlockChain;
import com.geeks.domain.profit.CoinPriceDetails;
import java.math.BigDecimal;
import java.util.Map;

public interface BitcoinStatsService {
    void update();
    void updateCurrency();
    BlockChain getBtcStats();
    Map<String, CoinPriceDetails> getCurrencyMap();
    Double rateCurrency(String key);
    Double calculateCoinPerDay(BigDecimal hashRate);
}
