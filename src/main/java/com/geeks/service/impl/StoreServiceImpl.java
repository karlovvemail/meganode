package com.geeks.service.impl;

import com.geeks.service.StoreService;
import com.geeks.domain.Location;
import com.geeks.domain.Machine;
import com.geeks.domain.Order;
import com.geeks.domain.User;
import com.geeks.domain.enumeration.OrderStatus;
import com.geeks.dto.StoreMinerDescriptionDTO;
import com.geeks.dto.StoreMinerListDTO;
import com.geeks.repository.LocationRepository;
import com.geeks.repository.MachineRepository;
import com.geeks.repository.OrdersRepository;
import com.geeks.repository.UsersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("storeService")
@lombok.extern.log4j.Log4j2
public class StoreServiceImpl implements StoreService {

    private final LocationRepository locationRepository;
    private final MachineRepository machineRepository;
    private final OrdersRepository ordersRepository;
    private final UsersRepository usersRepository;
    private final ModelMapper modelMapper;
    
    public StoreServiceImpl(LocationRepository locationRepository, MachineRepository machineRepository,
                            OrdersRepository ordersRepository, UsersRepository usersRepository, 
                            @Qualifier("modelMapper")ModelMapper modelMapper) {
        this.locationRepository = locationRepository;
        this.machineRepository = machineRepository;
        this.ordersRepository = ordersRepository;
        this.usersRepository = usersRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<StoreMinerListDTO> findAllMachines() {
        return machineRepository.findAll().stream()
                .map(machine -> modelMapper.map(machine, StoreMinerListDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public StoreMinerDescriptionDTO findMachineById(Long machine_id){
        return machineRepository.findById(machine_id)
                .map((machine) -> modelMapper.map(machine,StoreMinerDescriptionDTO.class))
                .orElse(null);
    }

    @Override
    public List<Location> findMachineLocations(Long machine_id) {
        return machineRepository.findById(machine_id).map(Machine::getLocations).orElse(null);
    }

    @Override
    public Order createOrder(Long machine_id, Long location_id, Long user_id) {
        Optional<Machine> machine = machineRepository.findById(machine_id);
        Optional<User> users = usersRepository.findById(user_id);
        Optional<Location> location = locationRepository.findById(location_id);
        if(!location.isPresent() || !users.isPresent() || !machine.isPresent()) return null;

        Order order = new Order();
        order.setMachine(machine.get());
        order.setPrice(machine.get().getPrice());
        order.setOrderStatus(OrderStatus.DRAFT);
        order.setDateStart(LocalDate.now());
        order.setDateEnd(LocalDate.now().plusYears(3));
        order.setSetupFeesSum(location.get().getSetupFees());
        order.setMonthlyPlan(location.get().getHostingFees() + location.get().getElectricityFees());
        order.setCount(1);
        order.setUser(users.get());
        order.setLocation(location.get());

        return ordersRepository.save(order);
    }
}
