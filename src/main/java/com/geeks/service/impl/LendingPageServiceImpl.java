package com.geeks.service.impl;

import com.geeks.domain.Location;
import com.geeks.domain.Machine;
import com.geeks.dto.LendingMainDTO;
import com.geeks.dto.LendingMinersDTO;
import com.geeks.dto.LendingProfitDTO;
import com.geeks.dto.MonthlyPlanDTO;
import com.geeks.dto.PastEarningsDTO;
import com.geeks.repository.LocationRepository;
import com.geeks.repository.MachineRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import com.geeks.service.BitcoinStatsService;
import com.geeks.service.LendingPageService;
import org.springframework.stereotype.Service;

/**
 *
 * @author alex
 */
@lombok.extern.log4j.Log4j2
@Service("lendingPageService")
public class LendingPageServiceImpl implements LendingPageService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private MachineRepository machineRepository;

    @Autowired
    private BitcoinStatsService bitcoinStats;

    @Override
    public LendingProfitDTO calculateProfitMiner(Long idMachine, Long idLocation, String currency) {
        
        final Machine machine = machineRepository.findById(idMachine).get();
        
        final Location location = locationRepository.findById(idLocation).get();
        
        final PastEarningsDTO pastEarningsDTO = PastEarningsDTO.builder()
                .inCome(bitcoinStats.calculateCoinPerDay(new BigDecimal(machine.getHashRate())))
                .elCost(location.getCostKw()*24/bitcoinStats.rateCurrency("USD"))
                .hostingFees(location.getHostingFees()/30/bitcoinStats.rateCurrency("USD"))
                .rate(bitcoinStats.rateCurrency(currency))
                .build();
        
        return new LendingProfitDTO(machine, location, pastEarningsDTO);
        
    }
    
    @Override
    public List<String> getListLocationName() {
        return locationRepository.findAll().stream()
                .map(Location::getName)
                .collect(Collectors.toList());
    }

    @Override
    public LendingMainDTO buildMainDTO() {

        return LendingMainDTO.builder()
                .planList(locationRepository.findAll().stream()
                        .map((location) -> {
                            return MonthlyPlanDTO.builder()
                                    .idLocation(location.getId())
                                    .locationHosting(location.getName())
                                    .elCost(location.getCostKw())
                                    .priceUnit(location.getMaintenanceFees())
                                    .setupFees(location.getSetupFees())
                                    .build();
                        })
                        .collect(Collectors.toList()))
                .lendingMinersDTO(machineRepository.findAll().stream()
                        .map((machine) -> {
                            return LendingMinersDTO.builder()
                                    .idMachine(machine.getId())
                                    .asicUnit(machine.getBrand() + machine.getModelType())
                                    .hashrate(machine.getHashRate())
                                    .price(machine.getPrice())
                                    .build();
                        })
                        .collect(Collectors.toList()))
                .build();
        
    }

}
