package com.geeks.service.impl;

import com.geeks.domain.CoinProfitPerDay;
import com.geeks.domain.CurrencyProfit;
import com.geeks.domain.Miner;
import com.geeks.domain.profit.BlockChain;
import com.geeks.domain.profit.CoinPriceDetails;
import com.geeks.repository.MinerRepository;
import com.geeks.service.BitcoinStatsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import org.springframework.stereotype.Service;

@lombok.extern.log4j.Log4j2
@Service("bitcoinStatsService")
public class BitcoinStatsServiceImpl implements BitcoinStatsService {
    
    private final static BigInteger MULTIPLY = BigInteger.valueOf((long) Math.pow(2, 32));
    
    private final BigDecimal time;
    
    private final RestTemplate restTemplate;

    private final MinerRepository minerRepository;
    
    @lombok.Getter(AccessLevel.PUBLIC)
    private BlockChain btcStats;
    
    @lombok.Getter(AccessLevel.PUBLIC)
    private Map<String, CoinPriceDetails> currencyMap;

    @Autowired
    public BitcoinStatsServiceImpl(MinerRepository minerRepository) {
        this.restTemplate = new RestTemplate();
        this.time = new BigDecimal(86400);
        this.minerRepository = minerRepository;
    }
    
    @Override
    public void update() {
        updateChainStats();
        updateMinerProfit();        
    }
    
    private void updateChainStats() {
        btcStats = restTemplate.getForObject("https://api-r.bitcoinchain.com/v1/status", BlockChain.class);
    }

    @Override
    public void updateCurrency(){
        Type itemsMapType = new TypeToken<Map<String, CoinPriceDetails>>() {}.getType();
        String currency = restTemplate.getForObject("https://blockchain.info/ticker", String.class);
        currencyMap = new Gson().fromJson(currency, itemsMapType);
        log.info(currencyMap.toString());
    }
    
    private void updateMinerProfit() {
        final List<Miner> miners = minerRepository.findAll().stream()
                .peek(miner -> {
                    CoinProfitPerDay coinProfitPerDay = new CoinProfitPerDay(
                            LocalDate.now(), 
                            calculateCoinPerDay(new BigDecimal(miner.getMachine().getHashRate()), 
                                    new BigDecimal(miner.getMachine().getPowerConsumption()), 
                                    new BigDecimal(miner.getLocation().getCostKw())),
                            miner);
                    CurrencyProfit currencyProfit = 
                            new CurrencyProfit(
                                    "USD", 
                                    (coinProfitPerDay.getProfit() * rateCurrency("USD")), 
                                    coinProfitPerDay);
                    coinProfitPerDay.addCurrencyProfits(currencyProfit);
                    miner.addCoinlogProfits(coinProfitPerDay);
                })
                .collect(Collectors.toList());
        minerRepository.saveAll(miners);
    }

    @Override
    public Double calculateCoinPerDay(BigDecimal hashRate) {
        return hashRate
                .multiply(new BigDecimal(btcStats.getReward()))
                .multiply(time)
                .multiply(new BigDecimal(1000000))
                .divide(new BigDecimal(new BigInteger(btcStats.getDifficulty())
                        .multiply(MULTIPLY)), MathContext.DECIMAL128) 
                .setScale(7, RoundingMode.DOWN)
                .doubleValue();
    }

    private Double calculateCoinPerDay(BigDecimal hashRate,BigDecimal powerConsumption, BigDecimal energyPrice) {
        return hashRate
                .multiply(new BigDecimal(btcStats.getReward()))
                .multiply(time)
                .multiply(new BigDecimal(1000000))
                .divide(new BigDecimal(new BigInteger(btcStats.getDifficulty())
                        .multiply(MULTIPLY)), MathContext.DECIMAL128)
                .multiply(new BigDecimal(rateCurrency("USD")))
                .add(powerConsumption
                        .multiply(energyPrice)
                        .multiply(new BigDecimal(24).negate()))
                .divide(new BigDecimal(rateCurrency("USD")), MathContext.DECIMAL128)
                .setScale(7, RoundingMode.DOWN)
                .doubleValue();
    }
    
    @Override
    public Double rateCurrency(String key) {
        return (Double)currencyMap.get(key).getLast();
    }
        
}