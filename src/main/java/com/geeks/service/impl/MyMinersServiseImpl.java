package com.geeks.service.impl;

import com.geeks.domain.User;
import com.geeks.dto.myminers.ActiveMinersDTO;
import com.geeks.dto.myminers.MyMinersDTO;
import com.geeks.dto.myminers.OFFMinersDTO;
import com.geeks.dto.myminers.PreOrderMinersDTO;
import com.geeks.repository.UsersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import java.util.Optional;
import static com.geeks.domain.enumeration.MinerStatus.*;
import com.geeks.service.MyMinersService;
import org.springframework.stereotype.Service;

@Service("myMinersServise")
@lombok.extern.log4j.Log4j2
public class MyMinersServiseImpl implements MyMinersService {
    
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    @Qualifier("modelMapper")
    private ModelMapper modelMapper;

    @Override
    public MyMinersDTO getMinersByUser(Long user_id) {
        
        Optional<User> user =  usersRepository.findById(user_id);

        if(!user.isPresent())return null;

        final MyMinersDTO myMinersDTO = new MyMinersDTO();
        
        user.get().getOrders().stream()
                .flatMap((order) -> order.getMiners().stream())
                .forEach(miner -> {
            switch (miner.getMinerStatus()){
                case ACITVE_MINING:
                    ActiveMinersDTO activeMinersDTO = modelMapper.map(miner,ActiveMinersDTO.class);
                    activeMinersDTO.setLast24Btc(miner.getCoinlogProfits().iterator().next().getProfit());
                    activeMinersDTO.setLast24Usd(miner.getCoinlogProfits().iterator().next().getCurrencyProfits().iterator().next().getProfit());
                    myMinersDTO.addActiveMiners(activeMinersDTO);
                    break;
                case PRE_ORDERED:
                    PreOrderMinersDTO preOrderMinersDTO = modelMapper.map(miner,PreOrderMinersDTO.class);
                    preOrderMinersDTO.setLocationName(miner.getLocation().getName());
                    preOrderMinersDTO.setMonthlyPlan(miner.getOrder().getMonthlyPlan());
                    myMinersDTO.addOrderMiners(modelMapper.map(miner,PreOrderMinersDTO.class));
                    break;
                case NO_ACTIVE:
                    myMinersDTO.addOffMiners(modelMapper.map(miner, OFFMinersDTO.class));
                    break;
            }
        });
        
        return myMinersDTO;
        
    }
    
}
