package com.geeks.service.impl;

import com.geeks.service.AwsSmsService;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("awsSmsService")
public class AwsSmsServiceImpl implements AwsSmsService {
    
    private final AmazonSNS snsClient;

    private final Map<String, MessageAttributeValue> smsAttributes;

    public AwsSmsServiceImpl(
            @Value("${cloud.aws.region.static}") String region,
            @Qualifier("amazonAWSCredentials") AWSCredentials amazonAWSCredentials) {

        snsClient = AmazonSNSClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider(amazonAWSCredentials))
                .withRegion(region)                
                .build();
        
        smsAttributes = new HashMap<>();
        
    }
    
    @Override
    public String send(String phoneNumber, int numbSize) {
        String message = getRandomNumber(numbSize);
        String messageID = send(phoneNumber, message);
        return message;
    }

    @Override
    public String send(String phoneNumber, String message) {
        
        final PublishResult publish = snsClient.publish(new PublishRequest()
                .withMessage(message)
                .withPhoneNumber(phoneNumber)
                .withMessageAttributes(smsAttributes));
        
        return publish.getMessageId();
        
    }
    
    private String getRandomNumber(int numbSize) {
        int index = 1;
        for (int i = 0; i < numbSize; i++) index *= 10;
        final String random = String.valueOf(index) + (int) (Math.random() * index);
        return random.substring(random.length() - numbSize);
    }
    
}
