package com.geeks.service;

import com.geeks.dto.LendingMainDTO;
import com.geeks.dto.LendingProfitDTO;
import java.util.List;

/**
 *
 * @author alex
 */
public interface LendingPageService {
    
    public LendingProfitDTO calculateProfitMiner(Long idMachine, Long idLocation, String currency);
    
    public List<String> getListLocationName();
    
    public LendingMainDTO buildMainDTO();
    
}
