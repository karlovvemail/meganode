package com.geeks;

import com.geeks.service.AwsSmsService;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.boot.CommandLineRunner;
/**
 *
 * @author _1000geeks/Kazakov_Dmitriy
 */
@SpringBootApplication(scanBasePackages = {"com.geeks"})
@EnableAutoConfiguration
@EnableScheduling
public class MeganodeApp extends SpringBootServletInitializer {

    private final static Logger log = LoggerFactory.getLogger(MeganodeApp.class);

    public static final String API_URL = "/api/v1.0";

    @Autowired
    private Environment env;
    
    public static void main(String[] args) {
        final Environment env = new SpringApplication(MeganodeApp.class).run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) protocol = "https";
        
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info("\n----------------------------------------------------------\n\t"
                + "Application is running! Access URLs:\n\t"
                + "Local: \t\t{}://localhost:{}\n\t"
                + "External: \t{}://{}:{}\n\t"
                + "Profile(s): \t{}\n----------------------------------------------------------",
                protocol,
                env.getProperty("server.port"),
                protocol,
                hostAddress,
                env.getProperty("server.port"),
                env.getActiveProfiles());
        
    }
    
    @Bean
    public CommandLineRunner initDB(AwsSmsService awsSmsService) {
        return (args) -> {
            //for (String bean : applicationContext.getBeanDefinitionNames()) System.out.println(bean);
            //addTestData.start();
            //awsSmsService.send("+380954629762", 4);
        };
    }
    
    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(MeganodeApp.class);
    }

    @Bean
    public AWSCredentials amazonAWSCredentials() {
        return new BasicAWSCredentials(env.getProperty("aws.access.key.id"),
                env.getProperty("aws.access.secret.key"));
    }
    
}
