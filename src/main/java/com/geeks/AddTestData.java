package com.geeks;

import com.geeks.domain.Location;
import com.geeks.domain.Machine;
import com.geeks.repository.LocationRepository;
import com.geeks.repository.MachineRepository;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author dmitriy
 */
@Component
public class AddTestData {
    
    @Autowired
    private MachineRepository mr;
    
    @Autowired
    private LocationRepository lr;
    
    public void start() {

        String locaionsJson = "[\n"
                + "    {\n"
                + "      \"id\": 1,\n"
                + "      \"name\": \"Canada, Quebec by Hydro Quebec\",\n"
                + "      \"img\": \"http://13.59.114.187/media/locations_images/2018/10/05/gASgSGxamkib.jpg\",\n"
                + "      \"cost_kw\": 0,\n"
                + "      \"monthly_plan_cost\": 0.03,\n"
                + "      \"electricity_fees\": 32,\n"
                + "      \"hosting_fees\": 10,\n"
                + "      \"maintenance_fees\": 8,\n"
                + "      \"setup_fees\": 0,\n"
                + "      \"life_duration\": 3,\n"
                + "      \"miners_count\": 0\n"
                + "    },\n"
                + "    {\n"
                + "      \"id\": 2,\n"
                + "      \"name\": \"USA, Montana\",\n"
                + "      \"img\": \"http://13.59.114.187/media/locations_images/2018/10/29/maxresdefault_1.jpg\",\n"
                + "      \"cost_kw\": 0.04,\n"
                + "      \"monthly_plan_cost\": 40,\n"
                + "      \"electricity_fees\": 10,\n"
                + "      \"hosting_fees\": 10,\n"
                + "      \"maintenance_fees\": 20,\n"
                + "      \"setup_fees\": 150,\n"
                + "      \"life_duration\": 3,\n"
                + "      \"miners_count\": 0\n"
                + "    },\n"
                + "    {\n"
                + "      \"id\": 3,\n"
                + "      \"name\": \"Iceland\",\n"
                + "      \"img\": \"http://13.59.114.187/media/locations_images/2018/10/29/rccl_destinationHeroSlider1.jpg\",\n"
                + "      \"cost_kw\": 0.05,\n"
                + "      \"monthly_plan_cost\": 60,\n"
                + "      \"electricity_fees\": 20,\n"
                + "      \"hosting_fees\": 20,\n"
                + "      \"maintenance_fees\": 20,\n"
                + "      \"setup_fees\": 200,\n"
                + "      \"life_duration\": 3,\n"
                + "      \"miners_count\": 0\n"
                + "    }\n"
                + "  ]\n";

        String machinesJson = "[\n"
                + "    {\n"
                + "      \"id\": 1,\n"
                + "      \"available\": true,\n"
                + "      \"img\": \"http://13.59.114.187/media/machine_images/2018/10/05/Asic-majner-Antminer-S9-500x313.jpg\",\n"
                + "      \"brand\": \"Antiminer\",\n"
                + "      \"model_type\": \"S9 j\",\n"
                + "      \"garanteed_uptime\": 99.98,\n"
                + "      \"power_consumption\": 1.452,\n"
                + "      \"hash_rate\": 14.5,\n"
                + "      \"decibel_squale\": \"0.00\",\n"
                + "      \"operation_temperature\": \"17\",\n"
                + "      \"storage_temperature\": \"18\",\n"
                + "      \"price\": \"700.00\",\n"
                + "      \"currency_mine\": 1,\n"
                + "      \"min_plan\": 0.03,\n"
                + "      \"roi\": 10\n"
                + "    },\n"
                + "    {\n"
                + "      \"id\": 2,\n"
                + "      \"available\": true,\n"
                + "      \"img\": \"http://13.59.114.187/media/machine_images/2018/10/05/s9-hydro.webp\",\n"
                + "      \"brand\": \"Antiminer\",\n"
                + "      \"model_type\": \"S9 Hydro\",\n"
                + "      \"garanteed_uptime\": 99.99,\n"
                + "      \"power_consumption\": 45,\n"
                + "      \"hash_rate\": 14,\n"
                + "      \"decibel_squale\": \"0.00\",\n"
                + "      \"operation_temperature\": \"20\",\n"
                + "      \"storage_temperature\": \"21\",\n"
                + "      \"price\": \"400.00\",\n"
                + "      \"currency_mine\": \"sha256\",\n"
                + "      \"min_plan\": 0.03,\n"
                + "      \"roi\": 10\n"
                + "    },\n"
                + "    {\n"
                + "      \"id\": 3,\n"
                + "      \"available\": true,\n"
                + "      \"img\": \"http://13.59.114.187/media/machine_images/2018/10/05/download.jpeg\",\n"
                + "      \"brand\": \"GMO\",\n"
                + "      \"model_type\": \"B2\",\n"
                + "      \"garanteed_uptime\": 99,\n"
                + "      \"power_consumption\": 4,\n"
                + "      \"hash_rate\": 17,\n"
                + "      \"decibel_squale\": \"0.00\",\n"
                + "      \"operation_temperature\": \"34\",\n"
                + "      \"storage_temperature\": \"35\",\n"
                + "      \"price\": \"800.00\",\n"
                + "      \"currency_mine\": 1,\n"
                + "      \"min_plan\": 40,\n"
                + "      \"roi\": 10\n"
                + "    }\n"
                + "  ]";

        Location[] locations = new Gson().fromJson(locaionsJson, Location[].class);
        System.out.println(Arrays.toString(locations));
        Machine[] machines = new Gson().fromJson(machinesJson, Machine[].class);
        System.out.println(Arrays.toString(machines));

        for (Location location : locations) {
            location.setId(null);
            lr.save(location);
        }
        for (Machine machine : machines) {
            machine.setId(null);
            mr.save(machine);
        }
        
        List<Location> locList = lr.findAll();
        List<Machine> machList = mr.findAll();
        
        locList.get(0).addMachine(machList.get(0));
        locList.get(0).addMachine(machList.get(1));
        locList.get(1).addMachine(machList.get(2));
        locList.get(2).addMachine(machList.get(0));
        locList.get(2).addMachine(machList.get(2));
        
        machList.get(0).addLocation(locList.get(0));
        machList.get(0).addLocation(locList.get(2));
        machList.get(1).addLocation(locList.get(0));
        machList.get(2).addLocation(locList.get(1));
        machList.get(2).addLocation(locList.get(2));
        
        lr.saveAll(locList);
        mr.saveAll(machList);
        
    }
    
}
