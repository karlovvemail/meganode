package com.geeks.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ModelMapperUtil {
    @Autowired
    private ModelMapper modelMapper;

    public <T,C> T map(C object,Class<T> dtoClass){
        return modelMapper.map(object,dtoClass);
    }

    public <T,C> List<T> mapList(List<C> object, Class<T> dtoClass){
        return object.stream().map(ob ->modelMapper.map(ob,dtoClass)).collect(Collectors.toList());
    }

}
