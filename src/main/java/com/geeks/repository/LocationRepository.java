package com.geeks.repository;

import com.geeks.domain.Location;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


/**
 * Spring Data  repository for the Location entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    @Query("select m.locations from Machine m where m.id = :id")
    List<Location> findByMachine(@Param("id") Long id);
}
