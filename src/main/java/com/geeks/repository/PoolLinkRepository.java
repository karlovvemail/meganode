package com.geeks.repository;

import com.geeks.domain.PoolLink;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PoolLink entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PoolLinkRepository extends JpaRepository<PoolLink, Long> {

}
