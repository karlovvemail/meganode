package com.geeks.repository;

import com.geeks.domain.MainLogProfit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MainLogProfit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MainLogProfitRepository extends JpaRepository<MainLogProfit, Long> {

}
