package com.geeks.repository;

import com.geeks.domain.Algorithm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Algorithm entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlgorithmRepository extends  JpaRepository<Algorithm, Long> {

}
