package com.geeks.repository;

import com.geeks.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Users entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    
    public User findByEmailAndPassword (String email, String password);
    
}
