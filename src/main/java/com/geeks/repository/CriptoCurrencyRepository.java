package com.geeks.repository;

import com.geeks.domain.CriptoCurrency;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CriptoCurrency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CriptoCurrencyRepository extends JpaRepository<CriptoCurrency, Long> {

}
