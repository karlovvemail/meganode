package com.geeks.repository;

import com.geeks.domain.CoinProfitPerDay;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CoinProfitPerDay entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoinProfitPerDayRepository extends JpaRepository<CoinProfitPerDay, Long> {

}
