package com.geeks.repository;

import com.geeks.domain.WalletCurrency;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WalletCurrency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletCurrencyRepository extends JpaRepository<WalletCurrency, Long> {

}
