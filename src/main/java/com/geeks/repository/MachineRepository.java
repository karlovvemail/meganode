package com.geeks.repository;

import com.geeks.domain.Machine;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Machine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MachineRepository extends JpaRepository<Machine, Long> {
/*
    @Query(value = "select distinct machine from Machine machine left join fetch machine.locations left join fetch machine.criptoCurrencies",
        countQuery = "select count(distinct machine) from Machine machine")
    Page<Machine> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct machine from Machine machine left join fetch machine.locations left join fetch machine.criptoCurrencies")
    List<Machine> findAllWithEagerRelationships();

    @Query("select machine from Machine machine left join fetch machine.locations left join fetch machine.criptoCurrencies where machine.id =:id")
    Optional<Machine> findOneWithEagerRelationships(@Param("id") Long id);
*/
}
