package com.geeks.repository;

import com.geeks.domain.LegalAgreement;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LegalAgreement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegalAgreementRepository extends JpaRepository<LegalAgreement, Long> {

}
