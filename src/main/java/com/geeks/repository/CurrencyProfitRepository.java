package com.geeks.repository;

import com.geeks.domain.CurrencyProfit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CurrencyProfit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CurrencyProfitRepository extends JpaRepository<CurrencyProfit, Long> {

}
