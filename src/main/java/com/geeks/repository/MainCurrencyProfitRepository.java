package com.geeks.repository;

import com.geeks.domain.MainCurrencyProfit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MainCurrencyProfit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MainCurrencyProfitRepository extends JpaRepository<MainCurrencyProfit, Long> {

}
