package com.geeks.repository;

import com.geeks.domain.Miner;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Miner entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface MinerRepository extends JpaRepository<Miner, Long> {
    
    //@Query("select u.miners from Users u where u.id = :id")
    //List<Miner> findByUser(@Param("id") Long id);
    
}
