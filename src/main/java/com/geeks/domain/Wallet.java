package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Wallet.
 */
@lombok.Data
@Entity
@Table(name = "wallet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Column(name = "bonus")
    private String bonus;

    @Column(name = "profit")
    private Double profit;

    @Column(name = "total_btc")
    private Double totalBtc;

    @Column(name = "is_auto_withdrawal")
    private Boolean isAutoWithdrawal;

    @Column(name = "auto_withdrawal")
    private Double autoWithdrawal;

    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<MainLogProfit> mainLogs = new ArrayList<>();
    
    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<WalletCurrency> currencies = new ArrayList<>();
    
    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Balance> balances = new ArrayList<>();
    
    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Miner> miners = new ArrayList<>();
    
    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Order> orders = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("wallets")
    private User user;
    
}
