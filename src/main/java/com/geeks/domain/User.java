package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.geeks.domain.enumeration.Role;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Users.
 */
@lombok.Data
@Entity
@Table(name = "users")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "email")
    private String email;
    
    @Column(name = "password")
    private String password;

    @Column(name = "data_joined")
    private String dataJoined;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "is_activave")
    private Boolean isActivave;

    @Column(name = "is_staff")
    private Boolean isStaff;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "pin")
    private String pin;

    @Column(name = "enabled_touch_id")
    private Boolean enabledTouchId;

    @Column(name = "ref_code")
    private String refCode;

    @Column(name = "paid")
    private Boolean paid;

    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Card> cards = new ArrayList<>();
    
    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Wallet> wallets = new ArrayList<>();
    
    @OneToMany(mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Order> orders = new ArrayList<>();
    
    @OneToMany(mappedBy = "referal")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<User> referrals = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("referrals")
    private User referal;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    public User() {
        this.role = Role.USER;
    }
    
}
