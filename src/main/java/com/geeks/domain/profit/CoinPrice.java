package com.geeks.domain.profit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoinPrice {
    
    @JsonProperty("USD")
    private CoinPriceDetails usd;

    @JsonProperty("AUD")
    private CoinPriceDetails aud;

    @JsonProperty("BRL")
    private CoinPriceDetails brl;

    @JsonProperty("CAD")
    private CoinPriceDetails cad;

    @JsonProperty("CHF")
    private CoinPriceDetails chf;

    @JsonProperty("CLP")
    private CoinPriceDetails clp;

    @JsonProperty("CNY")
    private CoinPriceDetails cny;

    @JsonProperty("DKK")
    private CoinPriceDetails dkk;

    @JsonProperty("EUR")
    private CoinPriceDetails eur;

    @JsonProperty("GBP")
    private CoinPriceDetails gbp;

    @JsonProperty("HKD")
    private CoinPriceDetails hkd;

    @JsonProperty("INR")
    private CoinPriceDetails inr;

    @JsonProperty("ISK")
    private CoinPriceDetails isk;

    @JsonProperty("JPY")
    private CoinPriceDetails jpy;

    @JsonProperty("KRW")
    private CoinPriceDetails krw;

    @JsonProperty("NZD")
    private CoinPriceDetails nzd;

    @JsonProperty("PIN")
    private CoinPriceDetails pln;

    @JsonProperty("RUB")
    private CoinPriceDetails rub;

    @JsonProperty("SEK")
    private CoinPriceDetails sek;

    @JsonProperty("SGD")
    private CoinPriceDetails sgd;

    @JsonProperty("THB")
    private CoinPriceDetails thb;
    
    @JsonProperty("TWD")
    private CoinPriceDetails twd;
    
}
