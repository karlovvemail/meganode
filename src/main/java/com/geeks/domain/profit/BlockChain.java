package com.geeks.domain.profit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlockChain {
    private int height;
    private String hash;
    private String prev_hash;
    private String difficulty;
    private String reward;
    private String next_block_reward;
    private long time;
}
