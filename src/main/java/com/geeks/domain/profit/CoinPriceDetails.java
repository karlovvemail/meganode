package com.geeks.domain.profit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoinPriceDetails {
    @JsonProperty("15m")
    private double updated;
    private double last;
    private double buy;
    private double sell;
    private String symbol;
}
