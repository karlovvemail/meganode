package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Card.
 */
@lombok.Data
@Entity
@Table(name = "card")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Column(name = "card_date")
    private LocalDate cardDate;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "cvc")
    private String cvc;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "jhi_number")
    private String number;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JsonIgnoreProperties("cards")
    private User user;

}
