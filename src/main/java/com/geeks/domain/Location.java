package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Location.
 */
@lombok.Data
@Entity
@Table(name = "location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "cost_kw")
    @SerializedName("cost_kw")
    private Double costKw;

    @Column(name = "img_url")
    @SerializedName("img")
    private String imgUrl;

    @Column(name = "setup_fees")  
    @SerializedName("setup_fees")
    private Double setupFees;

    @Column(name = "electricty_fees")
    @SerializedName("electricity_fees")
    private Double electricityFees;

    @Column(name = "hosting_fees")
    @SerializedName("hosting_fees")
    private Double hostingFees;

    @Column(name = "life_duration")
    @SerializedName("life_duration")
    private Integer lifeDuration;

    @Column(name = "maintenance_fees")
    @SerializedName("maintenance_fees")
    private Double maintenanceFees;

    @OneToMany(mappedBy = "location")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Miner> miners = new ArrayList<>();    
    
    @ManyToMany(mappedBy = "locations")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private List<Machine> machines = new ArrayList<>();
    
    public void addMachine(Machine machine) {
        machines.add(machine);
    }

}
