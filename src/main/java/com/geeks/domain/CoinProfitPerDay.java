package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A CoinProfitPerDay.
 */
@lombok.Data
@lombok.NoArgsConstructor
@Entity
@Table(name = "coin_profit_per_day")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CoinProfitPerDay implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "profit")
    private Double profit;

    @OneToMany(mappedBy = "coinProfitPerDay")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private final List<CurrencyProfit> currencyProfits = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("coinlogProfits")
    private Miner miner;

    public CoinProfitPerDay(LocalDate date, Double profit, Miner miner) {
        this.date = date;
        this.profit = profit;
        this.miner = miner;
    }
    
    public CoinProfitPerDay addCurrencyProfits (CurrencyProfit currencyProfit) {
        currencyProfits.add(currencyProfit);
        return this;
    }

}
