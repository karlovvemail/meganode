package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;

/**
 * A MainCurrencyProfit.
 */
@lombok.Data
@Entity
@Table(name = "main_currency_profit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MainCurrencyProfit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "profit")
    private Double profit;

    @ManyToOne
    @JsonIgnoreProperties("currencyProfits")
    private MainLogProfit mainLogProfit;
    
}
