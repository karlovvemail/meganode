package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A MainLogProfit.
 */
@lombok.Data
@Entity
@Table(name = "main_log_profit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MainLogProfit implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private String date;

    @Column(name = "profit")
    private Double profit;

    @OneToMany(mappedBy = "mainLogProfit")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<MainCurrencyProfit> currencyProfits = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("mainLogs")
    private Wallet wallet;
    
}
