package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import com.geeks.domain.enumeration.MinerStatus;
import java.util.ArrayList;
import java.util.List;

/**
 * A Miner.
 */
@lombok.Data
@Entity
@Table(name = "miner")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Miner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "assepted_shares")
    private Double asseptedShares;

    @Enumerated(EnumType.STRING)
    @Column(name = "miner_status")
    private MinerStatus minerStatus;

    @Column(name = "chip_temperature")
    private Double chipTemperature;

    @Column(name = "frequency")
    private Double frequency;

    @Column(name = "rejected_rate")
    private Double rejectedRate;

    @Column(name = "consumed_electricity")
    private Double consumedElectricity;

    @Column(name = "ip_address")
    private Double ipAddress;

    @Column(name = "total_btc")
    private Double totalBTC;

    @Column(name = "end_contract")
    private Double endContract;

    @Column(name = "power_status")
    private Boolean powerStatus;

    @Column(name = "server_up_time")
    private Double serverUpTime;

    @OneToMany(mappedBy = "miner")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<CoinProfitPerDay> coinlogProfits = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Order order;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Location location;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Pool pool;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Wallet wallet;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Machine machine;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private CriptoCurrency criptoCurrency;
    
    public Miner addCoinlogProfits(CoinProfitPerDay coinProfitPerDay) {
        this.coinlogProfits.add(coinProfitPerDay);
        coinProfitPerDay.setMiner(this);
        return this;
    }
    
}
