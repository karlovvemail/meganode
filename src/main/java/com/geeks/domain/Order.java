package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import com.geeks.domain.enumeration.OrderStatus;
import java.util.ArrayList;
import java.util.List;

/**
 * A Orders.
 */
@lombok.Data
@NoArgsConstructor
@Entity
@Table(name = "machine_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price")
    private Double price;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "order_status")
    private OrderStatus orderStatus;

    @Column(name = "count")
    private Integer count;

    @Column(name = "date_start")
    private LocalDate dateStart;

    @Column(name = "dete_end")
    private LocalDate dateEnd;

    @Column(name = "setup_fees_sum")
    private Double setupFeesSum;

    @Column(name = "monthly_plan")
    private Double monthlyPlan;

    @Column(name = "tx_hash")
    private String txHash;

    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Miner> miners = new ArrayList<>();
    
    @ManyToOne
    @JsonIgnoreProperties("orders")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Location location;

    @ManyToOne
    @JsonIgnoreProperties("orders")
    private Wallet wallet;

    @ManyToOne
    @JsonIgnoreProperties("miners")
    private Machine machine;

    @ManyToOne
    @JsonIgnoreProperties("machines")
    private PaymentMethod paymentMethod;
    
}
