package com.geeks.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    DRAFT, PAID, DONE
}
