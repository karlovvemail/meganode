package com.geeks.domain.enumeration;

/**
 * The MinerStatus enumeration.
 */
public enum MinerStatus {
    PRE_ORDERED, ACITVE_MINING, NO_ACTIVE, OFF
}
