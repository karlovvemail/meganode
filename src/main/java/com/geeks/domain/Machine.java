package com.geeks.domain;

import com.google.gson.annotations.SerializedName;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Machine.
 */
@lombok.Data
@Entity
@Table(name = "machine")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Machine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "available")
    private Boolean available;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model_type")
    @SerializedName("model_type")
    private String modelType;
    
    @Column(name = "image_url")
    @SerializedName("img")
    private String imageUrl;

    @Column(name = "decibel_squale")
    @SerializedName("decibel_squale")
    private Double decibelSquale;

    @Column(name = "description")
    private String description;

    @Column(name = "server_up_time")
    @SerializedName("server_up_time")
    private Double serverUpTime;

    @Column(name = "hash_rate")
    @SerializedName("hash_rate")
    private Double hashRate;

    @Column(name = "operation_temperature")
    @SerializedName("operation_temperature")
    private Double operationTemperature;

    @Column(name = "power_consumption")
    @SerializedName("power_consumption")
    private Double powerConsumption;

    @Column(name = "price")
    private Double price;

    @Column(name = "storage_temperature")
    @SerializedName("storage_temperature")
    private Double storageTemperature;

    @OneToMany(mappedBy = "machine")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Miner> miners = new ArrayList<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "machine_locations",
            joinColumns = @JoinColumn(name = "machines_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "locations_id", referencedColumnName = "id"))
    private List<Location> locations = new ArrayList<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "machine_cripto_currencies",
            joinColumns = @JoinColumn(name = "machines_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "cripto_currencies_id", referencedColumnName = "id"))
    private List<CriptoCurrency> criptoCurrencies = new ArrayList<>();
    
    public void addLocation(Location location) {
        locations.add(location);
    }

}
