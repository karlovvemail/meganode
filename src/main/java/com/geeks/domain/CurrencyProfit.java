package com.geeks.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;

/**
 * A CurrencyProfit.
 */
@lombok.Data
@lombok.NoArgsConstructor
@Entity
@Table(name = "currency_profit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CurrencyProfit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "profit")
    private Double profit;

    @ManyToOne
    @JsonIgnoreProperties("currencyProfits")
    private CoinProfitPerDay coinProfitPerDay;

    public CurrencyProfit(String name, Double profit, CoinProfitPerDay coinProfitPerDay) {
        this.name = name;
        this.profit = profit;
        this.coinProfitPerDay = coinProfitPerDay;
    }
    
}
