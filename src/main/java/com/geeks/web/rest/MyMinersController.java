package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.dto.myminers.MyMinersDTO;
import com.geeks.service.MyMinersService;
import com.geeks.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/my_miners")
public class MyMinersController {

    @Autowired
    private MyMinersService myMinersService;

    @GetMapping("/{user_id}")
    public ResponseEntity<MyMinersDTO> getUserMiners(@PathVariable Long user_id) {
        log.debug("REST request to get MyMiners : {}", user_id);
        MyMinersDTO myMinersDTO = myMinersService.getMinersByUser(user_id);

        if(myMinersDTO==null) {
            log.info("REST request to get MyMiners : {}", "Entity with id + " + user_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity with id + " + user_id + " not found"), HttpStatus.NOT_FOUND);
        }

        return  ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, user_id.toString()))
                .body(myMinersDTO);
    }
}
