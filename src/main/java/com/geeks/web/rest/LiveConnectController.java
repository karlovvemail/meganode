package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.domain.Miner;
import com.geeks.dto.LiveMinerPoolDTO;
import com.geeks.dto.MinerLiveConnectionDTO;
import com.geeks.repository.MinerRepository;
import com.geeks.util.HeaderUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/live_connect")
public class LiveConnectController {

    @Autowired
    private MinerRepository minerRepository;
    @Autowired
    @Qualifier("modelMapper")
    private ModelMapper modelMapper;

    @GetMapping("/{miner_id}")
    public ResponseEntity<MinerLiveConnectionDTO> getMinerLive(@PathVariable Long miner_id){
        Optional<Miner> miner = minerRepository.findById(miner_id);
        if(!miner.isPresent()){
            log.info("REST request to get MyMiners : {}", "Entity with id + " + miner_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity with id + " + miner_id + " not found"), HttpStatus.NOT_FOUND);
        }
        if(miner.get().getPowerStatus()) {
            MinerLiveConnectionDTO minerLive = modelMapper.map(miner.get(), MinerLiveConnectionDTO.class);
            minerLive.setServerUpTime(99.9);
            minerLive.setLocationName(miner.get().getLocation().getName());
            minerLive.setPool(modelMapper.map(miner.get().getPool(), LiveMinerPoolDTO.class));
            minerLive.setMonthlyPlan(miner.get().getOrder().getMonthlyPlan().toString());
            minerLive.setAcceptedShares(100-minerLive.getRejectedRate());
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, miner_id.toString()))
                    .body(minerLive);
        }else {
            MinerLiveConnectionDTO minerLive = MinerLiveConnectionDTO.builder()
                    .consumedElectricity(miner.get().getConsumedElectricity())
                    .endContract(miner.get().getEndContract())
                    .locationName(miner.get().getLocation().getName())
                    .pool(modelMapper.map(miner.get().getPool(), LiveMinerPoolDTO.class))
                    .monthlyPlan("On Hold")
                    .build();
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, miner_id.toString()))
                    .body(minerLive);
        }
    }
}
