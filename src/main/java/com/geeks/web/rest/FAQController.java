package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.domain.FAQ;
import com.geeks.repository.FAQRepository;
import com.geeks.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/faq")
public class FAQController {
    @Autowired
    private FAQRepository faqRepository;

    @GetMapping("/")
    public ResponseEntity<List<FAQ>> getFAQs() {
        log.debug("REST request to get all FAQ : {}");
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,"FAQ"))
                .body(faqRepository.findAll());
    }

    @GetMapping("/{faq_id}")
    public ResponseEntity<FAQ> getFAQ(@PathVariable Long faq_id) {
        log.debug("REST request to get FAQ : {}", faq_id);
        Optional<FAQ> faq = faqRepository.findById(faq_id);
        if(!faq.isPresent()){
            log.info("REST request to get FAQ : {}", "Entity with id  " + faq_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,"Invalid id " + faq_id), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,faq_id.toString()))
                .body(faq.get());
    }

}
