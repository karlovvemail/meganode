package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.domain.*;
import com.geeks.dto.HostingLocationDTO;
import com.geeks.dto.StoreMinerDescriptionDTO;
import com.geeks.dto.StoreMinerListDTO;
import com.geeks.dto.StoreOrderDTO;
import com.geeks.service.StoreService;
import com.geeks.util.HeaderUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/store")
public class StoreController {

    @Autowired
    @Qualifier("modelMapper")
    private ModelMapper modelMapper;
    @Autowired
    private StoreService storeService;


    @GetMapping("/machines")
    public ResponseEntity<List<StoreMinerListDTO>> getMachines(){
        log.debug("REST request to get All Machines");
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, "allMachines"))
                .body(storeService.findAllMachines());
    }

    @GetMapping("/machines/{machine_id}")
    public ResponseEntity<StoreMinerDescriptionDTO> getMachine(@PathVariable Long machine_id){
        log.debug("REST request to get Machine");
        StoreMinerDescriptionDTO minerDescriptionDTO = storeService.findMachineById(machine_id);
        if(minerDescriptionDTO == null){
            log.info("REST request to get Machine : {}", "Entity with id + " + machine_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity with id + " + machine_id + " not found"), HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, "allMachines"))
                .body(minerDescriptionDTO);
    }

    @GetMapping("/hosting/{machine_id}")
    public ResponseEntity<List<HostingLocationDTO>> getHostingLocation(@PathVariable Long machine_id) {
        log.debug("REST request to get HostingLocation : {}", machine_id);
        List<Location> location = storeService.findMachineLocations(machine_id);
        if(location.isEmpty() ) {
            log.info("REST request to get HostingLocation : {}", "Entity with id + " + machine_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity with id  " + machine_id + " not found"), HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, machine_id.toString()))
                .body(location.stream().map(this::locationToDTO).collect(Collectors.toList()));
    }

    private HostingLocationDTO locationToDTO(Location location){
        HostingLocationDTO hostingLocationDTO = modelMapper.map(location,HostingLocationDTO.class);
        hostingLocationDTO.setElectricityAndHostingFees(location.getHostingFees() + location.getElectricityFees());
        return  hostingLocationDTO;
    }

    @RequestMapping(value = "/order/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StoreOrderDTO> getOrder(@RequestParam Long user_id, @RequestParam Long machine_id, @RequestParam Long location_id) {
        log.debug("REST request to get Order : {},{},{}", user_id,machine_id,location_id);
        Order order = storeService.createOrder(machine_id,location_id,user_id);

        if(order==null){
            log.info("REST request to get HostingLocation : {}", "Entity with id + " + user_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "User with id "+user_id+",location with id "+location_id+" or machine with id " + machine_id + "not found "), HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, order.getId().toString()))
                .body(modelMapper.map(order,StoreOrderDTO.class));
    }

}
