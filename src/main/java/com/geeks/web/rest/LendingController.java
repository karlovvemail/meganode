package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.dto.LendingMainDTO;
import com.geeks.dto.LendingProfitDTO;
import com.geeks.util.HeaderUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.geeks.service.LendingPageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author alex
 */
@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL + "/lending")
public class LendingController {
    
    private static final String ENTITY_NAME = "Lending";

    @Autowired
    private LendingPageService lendingService;

    @GetMapping("/locations")
    public ResponseEntity<?> getLocations() {
        
        log.debug("REST request to get all Location");
        
        List<String> locationNameList = lendingService.getListLocationName();
        if (locationNameList == null) {
            log.info("REST request to get Balance : {}", "Entity locationNameList not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity locationNameList" + " not found"), HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, locationNameList.toString()))
                .body(locationNameList);
    }

    @GetMapping("/minerslocations")
    public ResponseEntity<LendingMainDTO> getAllMachins() {
        LendingMainDTO lendingMainDTO = lendingService.buildMainDTO();
        
        if (lendingMainDTO == null) {
            log.info("REST request to get Balance : {}", "Entity  + " + lendingMainDTO.toString() + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity + " + lendingMainDTO + " not found"), HttpStatus.NOT_FOUND);
        }

        else return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, lendingMainDTO.toString()))
                .body(lendingMainDTO);

    }

    @GetMapping("/profit")
    public ResponseEntity<LendingProfitDTO> calculateProfit(@RequestParam Long idMachine,
            @RequestParam Long idLocation, @RequestParam String currency, @RequestParam Double elCost) {
        
        LendingProfitDTO lendingProfitDTO = lendingService.calculateProfitMiner(idMachine, idLocation, currency);
        if (lendingProfitDTO == null) {
            log.info("REST request to get Balance : {}", "Entity with id + " + idMachine + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,
                    "Entity with id + " + idMachine + " not found"), HttpStatus.NOT_FOUND);
        }

        else return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityGetAlert(ENTITY_NAME, idMachine.toString()))
                .body(lendingProfitDTO);
    }
    
}
