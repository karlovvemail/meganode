package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.domain.LegalAgreement;
import com.geeks.repository.LegalAgreementRepository;
import com.geeks.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/legal_agreement")
public class LegalController {

    @Autowired
    private LegalAgreementRepository faqRepository;

    @GetMapping("/")
    public ResponseEntity<List<LegalAgreement>> getLegals() {
        log.debug("REST request to get all Legal : {}");
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,"Legal"))
                .body(faqRepository.findAll());
    }

    @GetMapping("/{legal_id}")
    public ResponseEntity<LegalAgreement> getLegal(@PathVariable Long legal_id) {
        log.debug("REST request to get Legal : {}", legal_id);
        Optional<LegalAgreement> legal = faqRepository.findById(legal_id);
        if(!legal.isPresent()){
            log.info("REST request to get Legal : {}", "Entity with id  " + legal_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,"Invalid id " + legal_id), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,legal_id.toString()))
                .body(legal.get());
    }

}
