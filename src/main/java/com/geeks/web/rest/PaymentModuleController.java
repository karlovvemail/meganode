package com.geeks.web.rest;

import com.geeks.MeganodeApp;
import com.geeks.domain.Order;
import com.geeks.domain.Wallet;
import com.geeks.domain.enumeration.OrderStatus;
import com.geeks.dto.PaymentModuleDTO;
import com.geeks.dto.TxInfoDTO;
import com.geeks.repository.OrdersRepository;
import com.geeks.service.BitcoinStatsService;
import com.geeks.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@lombok.extern.log4j.Log4j2
@RestController
@RequestMapping(MeganodeApp.API_URL+"/mobile/payment_module")
public class PaymentModuleController {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    @Qualifier("bitcoinStatsService")
    private BitcoinStatsService bitcoinStats;

    @GetMapping("/{order_id}")
    public ResponseEntity<PaymentModuleDTO> getPayment(@PathVariable Long order_id) {
        log.debug("REST request to get Order : {}", order_id);
        Optional<Order> order = ordersRepository.findById(order_id);
        if(!order.isPresent()){
            log.info("REST request to get PaymentInfo : {}", "Entity with id  " + order_id + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,"Invalid id " + order_id), HttpStatus.BAD_REQUEST);
        }
        Double available =(Double) order.get().getUser().getWallets().stream().mapToDouble(Wallet::getTotalBtc).sum();

        PaymentModuleDTO paymentModuleDTO = PaymentModuleDTO.builder()
            .availableBtc(available)
            .btcPrice((Double)bitcoinStats.getCurrencyMap().get("USD").getLast())
            .btcAmount(order.get().getPrice())
            .usdAmount(order.get().getPrice()*(Double)bitcoinStats.getCurrencyMap().get("USD").getLast())
            .build();
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,order_id.toString()))
                .body(paymentModuleDTO);
    }

    @GetMapping("/wait")
    public ResponseEntity<TxInfoDTO> waitingPayment(@RequestParam Long user_id,@RequestParam Long order_id,@RequestParam String txHash){
        Optional<Order> order = ordersRepository.findByTxHash(txHash);
        if(!order.isPresent()){
            log.info("REST request to get PaymentInfo : {}", "Entity with hash  " + txHash + " not found");
            return new ResponseEntity<>(HeaderUtil.createFailureAlert(ENTITY_NAME,"Invalid hash " + txHash), HttpStatus.BAD_REQUEST);
        }
        order.get().setOrderStatus(OrderStatus.PAID);
        ordersRepository.save(order.get());
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,txHash))
                .body(new TxInfoDTO(order.get().getId(),order.get().getUser().getId()));
    }

}