package com.geeks.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.geeks.service.BitcoinStatsService;

@Component
public class Sheduler {

    @Autowired
    @Qualifier("bitcoinStatsService")
    private BitcoinStatsService bitcoinStats;
    
    @Scheduled(cron="0 0 1 * * *",zone = "Europe/Paris")
    public void updateBitcoinStats() {
        bitcoinStats.update();
    }

}
