package com.geeks.config;

import com.geeks.MeganodeApp;
import com.geeks.domain.enumeration.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    
    @Autowired
    private AuthProvider authProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers(MeganodeApp.API_URL + "/lending/**").permitAll()
                .antMatchers(MeganodeApp.API_URL + "/mobile/**").authenticated()
                //.antMatchers("/mobile/**", "/lending/**").hasRole("ADMIN")
                //.antMatchers("/mobile/**", "/lending/**").hasAnyRole("USER", "ADMIN")
                .and().formLogin()
                //.successHandler(mySuccessHandler)
                //.failureHandler(myFailureHandler)
                .and().logout()
                .and().httpBasic();
        
    }
    
    @Override
    public void configure (AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider)
                .inMemoryAuthentication()
                .withUser("user").password("{noop}password").roles(Role.USER.name())
                .and()
                .withUser("admin").password("{noop}password").roles(Role.ADMIN.name());
    }
    
    /*
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password(encoder().encode("adminPass")).roles(Role.ADMIN.name())
                .and()
                .withUser("user").password(encoder().encode("userPass")).roles(Role.USER.name());
    }
    
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPassowordEncoder();
    }
*/
}
