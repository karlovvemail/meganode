package com.geeks.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Админ
 */
@Configuration
public class BeanConfig {
    
    @Bean("modelMapper")
    public ModelMapper getModelMapper (){
        return new ModelMapper();
    }
    
}
