package com.geeks.config;

import com.geeks.domain.User;
import com.geeks.domain.enumeration.Role;
import com.geeks.repository.UsersRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 *
 * @author Админ
 */
@Component
public class AuthProvider implements AuthenticationProvider{
    
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String email = authentication.getName();
        final String password = authentication.getCredentials().toString();
        final User user = usersRepository.findByEmailAndPassword(email, password);
        if (user==null) return null;
        else {
            final List <GrantedAuthority> grantedAuthority = new ArrayList <>(1);
            if (user.getRole()==null) grantedAuthority.add(new SimpleGrantedAuthority(Role.USER.name()));
            else grantedAuthority.add(new SimpleGrantedAuthority(user.getRole().name()));
            return new UsernamePasswordAuthenticationToken(email, password, grantedAuthority);
        }
    }
    
    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }
    
}
