/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geeks.dto;

import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author alex
 */
@Data
@Builder
public class LendingMainDTO {

    private List<MonthlyPlanDTO> planList;
    private List<LendingMinersDTO> lendingMinersDTO;
    
}
