package com.geeks.dto;

import lombok.Data;

@Data
public class StoreMinerDescriptionDTO {
    private Long id;
    private Double price;
    private String description;
    private Boolean available;
    private String brand;
    private Double powerConsumption;
    private Double decibelSquale;
}
