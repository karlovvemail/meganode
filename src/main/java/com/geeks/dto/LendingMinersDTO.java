package com.geeks.dto;

/**
 *
 * @author alex
 */
@lombok.Data
@lombok.Builder
public class LendingMinersDTO {
    private Long idMachine;
    private String asicUnit;
    private Double hashrate;
    private Double price;
}
