package com.geeks.dto;

/**
 *
 * @author alex
 */
@lombok.Data
public class ProfitCurrencyDTO {
    private Double btcPrice;
    private Double normalPrice;

    public ProfitCurrencyDTO(Double btcPrice, Double normalPrice, int dayNumb) {
        this.btcPrice = btcPrice*dayNumb;
        this.normalPrice = normalPrice*dayNumb;
    }
    
}

