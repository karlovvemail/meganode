package com.geeks.dto;

import com.geeks.domain.PoolLink;

import java.util.ArrayList;
import java.util.List;

public class LiveMinerPoolDTO {
    private String name;
    private String imgUrl;
    private List<PoolLink> links = new ArrayList<>();
}
