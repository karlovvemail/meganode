/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geeks.dto;

/**
 *
 * @author alex
 */
public class MinerDescriptionDTO {
    private String name;
    private Double  elCost;
    private String minerImgUrl;
    private String locationImgUrl;
    private Double hostingFees;
}
