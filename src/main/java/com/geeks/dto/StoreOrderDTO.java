package com.geeks.dto;

import com.geeks.domain.CriptoCurrency;
import com.geeks.domain.Location;

import java.time.LocalDate;

@lombok.Data
public class StoreOrderDTO {
    private String location;
    private Double hashRate;
    private Double powerConsumption;
    private CriptoCurrency criptoCurrency;
    private LocalDate startDate;
    private LocalDate endDate;
    private Double price;
    private Double setupFee;
    private Double monthlyPlan;

}
