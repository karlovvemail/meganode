package com.geeks.dto;

import lombok.Data;

@Data
public class HostingLocationDTO {
    private Long id;
    private Double costKw;
    private Double electricityAndHostingFees;
    private Double maintenanceFees;
}
