package com.geeks.dto;

/**
 *
 * @author dmitriy
 */
@lombok.Data
@lombok.Builder
public class PastEarningsDTO {
    
    private Double inCome; 
    private Double elCost; 
    private Double hostingFees; 
    private Double rate;
    
}
