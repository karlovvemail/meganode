package com.geeks.dto;

/**
 *
 * @author alex
 */
@lombok.Data
public class ProfitDTO {

    private ProfitCurrencyDTO inCome;
    private ProfitCurrencyDTO elCost;
    private ProfitCurrencyDTO hostingFees;
    private ProfitCurrencyDTO profit;

    public ProfitDTO(PastEarningsDTO pastEarningsDTO, int dayNumb) {
        
        this.inCome = new ProfitCurrencyDTO(pastEarningsDTO.getInCome(), pastEarningsDTO.getInCome()*pastEarningsDTO.getRate(), dayNumb);
        this.elCost = new ProfitCurrencyDTO(pastEarningsDTO.getElCost(), pastEarningsDTO.getElCost()*pastEarningsDTO.getRate(), dayNumb);
        this.hostingFees = new ProfitCurrencyDTO(pastEarningsDTO.getHostingFees(), pastEarningsDTO.getHostingFees()*pastEarningsDTO.getRate(), dayNumb);
        
        Double profit = pastEarningsDTO.getInCome() - pastEarningsDTO.getElCost() - pastEarningsDTO.getHostingFees();
        this.profit = new ProfitCurrencyDTO(profit, profit*pastEarningsDTO.getRate(), dayNumb);
        
    }
    
}
