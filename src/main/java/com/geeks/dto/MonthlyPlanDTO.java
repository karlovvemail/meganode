/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geeks.dto;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author alex
 */
@Data
@Builder
public class MonthlyPlanDTO {
    private Long idLocation;
    private String locationHosting;
    private Double elCost;
    private Double priceUnit;
    private Double setupFees;
}
