package com.geeks.dto;

import com.geeks.domain.CriptoCurrency;
import com.geeks.domain.Location;

@lombok.Data
public class MinersStoreInfoDTO {
    private Location location;
    private Double hashRate;
    private Double powerConsumption;
    private CriptoCurrency criptoCurrency;
    private Double startDate;
    private Double endDate;
    private Double price;
    //private Double setupFee;
    //private Double monthlyPlan;
    
}
