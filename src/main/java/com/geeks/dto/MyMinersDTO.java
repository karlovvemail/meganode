package com.geeks.dto;

import com.geeks.domain.CoinProfitPerDay;
import com.geeks.domain.Location;
import com.geeks.domain.enumeration.MinerStatus;
import java.util.Set;

@lombok.Data
public class MyMinersDTO {
    private String brand;
    private MinerStatus minerStatus;
    private Double hashRate;
    private Location location;
    private Set<CoinProfitPerDay> coinlogProfits;
}
