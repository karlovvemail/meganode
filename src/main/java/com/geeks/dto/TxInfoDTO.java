package com.geeks.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TxInfoDTO {
    private Long user_id;
    private Long order_id;
}
