package com.geeks.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MinerLiveConnectionDTO {
    private Boolean powerStatus;
    private Double serverUpTime;
    private Double chipTemperature;
    private Double hashRate;
    private Double frequency;
    private Double acceptedShares;
    private Double rejectedRate;
    private Double consumedElectricity;
    private Double ipAddress;
    private Double endContract;
    private Double totalBTC;
    private String locationName;
    private LiveMinerPoolDTO pool;
    private String monthlyPlan;
}
