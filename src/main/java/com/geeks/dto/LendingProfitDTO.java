package com.geeks.dto;

import com.geeks.domain.Location;
import com.geeks.domain.Machine;

/**
 *
 * @author alex
 */
@lombok.Data
public class LendingProfitDTO {

    private final ProfitDTO incomeOne;
    private final ProfitDTO incomeWeek;
    private final ProfitDTO incomeMonth;

    private final String machinegUrlImg;
    private final String hostingUrlImg;

    private final String machineName;
    private final String locationName;

    private final Double elCost;
    private final Double hostingFees;
    
    public LendingProfitDTO(Machine machine, Location location, PastEarningsDTO pastEarningsDTO) {
        
        this.incomeOne = new ProfitDTO(pastEarningsDTO, 1);
        this.incomeWeek = new ProfitDTO(pastEarningsDTO, 3);
        this.incomeMonth = new ProfitDTO(pastEarningsDTO, 7);
        
        this.machinegUrlImg = machine.getImageUrl();
        this.hostingUrlImg = location.getImgUrl();
        this.machineName = machine.getBrand() + machine.getModelType();
        this.locationName = location.getName();
        this.elCost = location.getCostKw();
        this.hostingFees = location.getHostingFees();
        
    }
    
}
