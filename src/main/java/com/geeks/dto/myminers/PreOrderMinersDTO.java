package com.geeks.dto.myminers;

import com.geeks.domain.enumeration.MinerStatus;
import lombok.Data;

@Data
public class PreOrderMinersDTO {
    private Long id;
    private String brand;
    private MinerStatus minerStatus;
    private Double hashRate;
    private String locationName;
    private Double monthlyPlan;

}
