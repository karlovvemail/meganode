package com.geeks.dto.myminers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.geeks.domain.CoinProfitPerDay;
import com.geeks.domain.enumeration.MinerStatus;
import lombok.Data;

import java.util.Set;

@Data
public class ActiveMinersDTO {
    private Long id;
    private String brand;
    private MinerStatus minerStatus;
    private Double hashRate;
    private Double last24Btc;
    private Double last24Usd;
    private Double totalBTC;
    private Double totalUsd;
    @JsonIgnore
    private Set<CoinProfitPerDay> coinlogProfits;
}
