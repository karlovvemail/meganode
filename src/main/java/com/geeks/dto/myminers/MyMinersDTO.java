package com.geeks.dto.myminers;

import java.util.ArrayList;
import java.util.List;

@lombok.Data
public class MyMinersDTO {
    private List<ActiveMinersDTO> activeMinersDTO = new ArrayList<>();
    private List<OFFMinersDTO> offMinersDTO = new ArrayList<>();
    private List<PreOrderMinersDTO> preOrderMinersDTO = new ArrayList<>();

    public void addOffMiners(OFFMinersDTO offMiner){
        offMinersDTO.add(offMiner);
    }
    public void addActiveMiners(ActiveMinersDTO activeMiner){
        activeMinersDTO.add(activeMiner);
    }
    public void addOrderMiners(PreOrderMinersDTO orderMiner){
        preOrderMinersDTO.add(orderMiner);
    }

}
