package com.geeks.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentModuleDTO {

    private Double availableBtc;
    private Double btcPrice;
    private Double usdAmount;
    private Double btcAmount;

}
