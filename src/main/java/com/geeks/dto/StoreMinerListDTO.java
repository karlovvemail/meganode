package com.geeks.dto;

@lombok.Data
public class StoreMinerListDTO {
    private Boolean available;
    private String brand;
    private String description;
    private Double price;
}
